const immutable = require('immutable')

const player = {
  getTrack (time) {
    const track = this.list.find((track) => time>=track.from && time<=track.to)
    if (track !== undefined) {
      return {
        track:track.track,
        time:(time - track.from)
      }
    } 
  },
  setTrack (from, to, track) {
    return module.exports(this.list.push({
      from: from, 
      to: to,
      track:track
    }).sortBy((item) => item.from))
  },
  start (time) {
    return this.setTrack(time, time, {})
  },
  setNextTrack (track, duration) {
    const endTime = this.list.last() ? this.list.last().to : 0
    return this.setTrack(endTime, (endTime + duration), track)
  },
  tail (count) {
    count = count || 10
    return this.list.slice(this.list.size-count, this.list.size)
      .map(t => t.track)
      .filter(t => t.id !== undefined)
  }

}

module.exports = (playlist) => {
  var play = Object.create(player)
  play.list = playlist || immutable.List()
  return play
  
}

