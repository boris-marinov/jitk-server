const q = require('q')
const tracks = [
/*  { duration: 30000, title: "Track 1", id:"1"},
  { duration: 30000, title: "Track 2", id:"2"},
  { duration: 30000, title: "Track 3", id:"3"},
  { duration: 30000, title: "Track 4", id:"4"},
  { duration: 30000, title: "Track 5", id:"5"},
  { duration: 30000, title: "Track 6", id:"6"},
  { duration: 30000, title: "Track 7", id:"7"}, */
  { duration: 255000, title: "Speak low", id:"B0AmLosfZA8"},
  { duration: 186600, title: "Rhymes Like Dimes", id:"h8KRpPkzcAk"},
  { duration: 162000, title: "MF DOOM - Hooks is extra", id:"uNEDj7eKv7M"},
  { duration: 162000, title: "waldeck make my day", id:"1rPvSC5UqLk"},
  { duration: 162000, title: "Muse - Supermassive Black Hole (Original Music Video)", id:"pta-gf6JaHQ"},
  { duration: 116200, title: "Fang Island - Daisy", id:"EIurAP4yHtQ"},
  { duration: 394200, title: "1983", id:"CybO7XtYpdU"}

]


exports.getInfo = (trackId) => {
  return q(tracks.find((track)=>track.id === trackId))
}

exports.search = (query) => {
  return tracks.filter((tr) => tr.indexOf(query) !== -1)
}

exports.initialTracks = () => tracks

