const minSuggestions = 10
const immutable = require('immutable')

const fillSuggestions = (suggestions, history)=>{
  suggestions = suggestions || immutable.Map()
  const missingSuggestions = minSuggestions - suggestions.size
  return history
    .filter((track) => suggestions.get(track.id) === undefined)
    .take(missingSuggestions)
    .reduce((suggestions, track) => suggestions.set(track.id, {votes: 0, track: track}), suggestions)
}

const votes = {
  vote (voted, votes) {
    votes = votes !== undefined ? votes : 1
    if(voted.id === undefined){throw "Suggested tracks must have and id"}
    const selected = this.suggestions.get(voted.id)
    const totalVotes = selected === undefined ? votes : selected.votes + votes
    const suggestionsUpdated = this.suggestions.set(voted.id, { track: voted, votes: totalVotes})
    console.log('Someone voted for '+voted.id)
    return module.exports(this.history, suggestionsUpdated)
  },
  get () {
    return this.suggestions
  },
  select () {
    return this.suggestions.toList().sortBy(s=>-s.votes).get(0).track
  },
  clear (history) {
    const selected = this.select()
    return module.exports(this.history, this.suggestions.delete(selected.id))
  }
}



module.exports = (history, suggestions) => {
  const myVotes = Object.create(votes)
  myVotes.history = history || immutable.List()
  myVotes.suggestions = fillSuggestions(suggestions, myVotes.history)
  return myVotes
}
