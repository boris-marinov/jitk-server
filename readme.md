About this program
==================

My idea about this project is to create an online service which allows users to create interactive TV channels, based on content from youtube. TV channels, because the played content is synchronised between each user, interactive, because users will be able to vote for which track plays next.


Using
=====

1. Start it with 'npm start'
2. Fire a browser and start 'http://localhost:4080/somechannel', where 'somechannel' is the name of your channel. This creates a new channel and starts playing predefined content.
3. Click on one of the preloaded tracks to place a vote for it. The track with the most votes will be played next.
4. Share the channel URL with someone else or open a second browser window. He will hear the same track as you.

TODO
====
* Add youtube search.
* Don't allow for a user to vote for the same track twice.

Known Issues
============
* Sometimes when a channel is created, the UI stalls.
* Videos lag when the UI tab is not focused.

File structure
==============
`client` folder contains the code that runs on the client, `server` contains the code that runs on the server, and the two have a similar structure:

  * `player` - schedules new videos and keeps the playlist updated.
  * `votes` - track how many votes each track has and generates new votea.
  * `index` - glue code, which wires all components together.
The client code also contains a patched fork of `react-youtube-player` npm package (maybe I will create a pull request, when I am done tweaking it:) ).
