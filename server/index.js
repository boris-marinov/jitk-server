const server = require('http').createServer()
const url = require('url')
const WebSocketServer = require('ws').Server
const wss = new WebSocketServer({ server: server })
const express = require('express')
const app = express()
const _ = require('lodash')
const port = 4080
const immutable = require('immutable')

//const youTube = require('./server/youtube')
const youTube = require('./mock-youtube')
const player = require('./player')
const votes = require('./votes')

const now = () => new Date().getTime()
const firstTrack = { duration: 255000, title: "Speak low", id:"B0AmLosfZA8"}
app.set('view engine', 'jade')


const createChannel = (id) => {
  const initialVotes = youTube.initialTracks().reduce((votes, track)=>{
    return votes.vote(track) 
  }, votes())

  return updateChannel({player:player().start(now()), votes:initialVotes, id:id, subscribers:immutable.List()})
}



const addSubscriber = (channel, connection, onclose) => {
  const id = channel.id
  channel.subscribers = channel.subscribers.push(connection)
  updateChannel(channel)
  connection.on('close', ()=> {
    const channel = getChannel(id)
    channel.subscribers = channel.subscribers.remove(channel.subscribers.indexOf(connection))
    updateChannel(channel)
  })
  connection.on('message', (msg)=> {
    const track = JSON.parse(msg)
    debugger
    const channel = getChannel(id)
    channel.votes = channel.votes.vote(track)
    updateChannel(channel)
  })
  return channel
}

var channels = {}

const getChannel = (id) => {
  return channels[id] || createChannel(id)
}
const getChannels = () => {
  return _.values(channels)
}

const serializeChannel = (channel) => {
  return JSON.stringify({player: channel.player.getTrack(now()), votes: channel.votes.get()})
}
const updateChannel = (channel) => {
  channel.subscribers.forEach((ws) => {
    if(ws.readyState === ws.OPEN){ws.send(serializeChannel(channel))}
  })
  channels[channel.id] = channel
  return channel
}

setInterval(() => {
  getChannels().forEach((channel) => {
    if (channel.player.getTrack(now() + 10000) === undefined) {
      const nextTrack = channel.votes.select() 
      console.log("Scheduling a new track: "+ nextTrack.id)
      const playerNew = channel.player.setNextTrack(nextTrack, nextTrack.duration)
      const votesNew = channel.votes.clear(playerNew.tail())
      return updateChannel({player: playerNew, votes: votesNew, id:channel.id, subscribers: channel.subscribers})
    } else {
      //TODO: remove
      updateChannel(channel)
    }
  })
}, 1000)
wss.on('connection', function connection(ws) {
  const channelName = url.parse(ws.upgradeReq.url, true).path.slice(1)
  console.log("Subscribing for "+channelName)
  addSubscriber(getChannel(channelName), ws)

})


app.use('/', express.static('public'))

app.get('/:channelname', (req, res) => {
  const channelName = req.params.channelName
  return res.render('../assets/index.jade', getChannel(channelName))
})

server.on('request', app);
server.listen(port, function () { console.log('Listening on ' + server.address().port) });
