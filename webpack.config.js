var webpack = require("webpack");
module.exports = {
    devtool:'inline-source-map',
    entry: "./client/index.js",
    output: {
        path: "./public/",
        filename: "jitk-server.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },

            {
              test: /\.jsx?$/,
              exclude: /(node_modules|bower_components)/,
              loader: 'babel',
              query: {
                presets: ['react', 'es2015', 'stage-0']
              }
            }               
        ]
    },
    plugins: [
      new webpack.OldWatchingPlugin()
    ]

};
