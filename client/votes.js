const React = require('react')
const _ = require('lodash')

const Row = (props) =>
  <div className='row'>{
    _.chunk(props.tracks, 5).map((chunk, i) =>
       <Chunk key={i} tracks={chunk} vote={props.vote}/>)
  }</div>

const Chunk = (props) =>
  <div className='col-md-4'>
    <div className="list-group">
      {props.tracks.map((track) =>
        <Track track={track} key={track.track.id}
        vote={() => props.vote(track.track)}
        />)}
    </div>
  </div>
  

const Track = (props) =>
  <button type='button' className='list-group-item' key={props.track.track.id} onClick={props.vote}> 
      {props.track.track.title} 
      <span className='badge'>{props.track.votes}</span>
  </button>

module.exports = React.createClass ({
  render () {
    return <div>{
      <Row tracks ={_.sortBy(_.values(this.props.tracks), 'votes').reverse()} vote={this.props.vote}/>
    }</div>
  }
})

