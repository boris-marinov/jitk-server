if ( global.v8debug ) {
	global.v8debug.Debug.setBreakOnException(); // speaks for itself
}
const suggestions = require('../server/votes')
const immutable = require('immutable')

exports.suggestions = function(test){
  var content = immutable.List([
    {id:1, title:'Chosen Item'}
  ])
  

  const chosenItem = content.get(0)
  const newItem = {id:12, title:'new Item'}

  const s = suggestions(content)
  
  sNew = s.vote(newItem)
  sChosen = s.vote(chosenItem)
  
  test.equal(sChosen.select(), chosenItem, 'Vote for an existing suggestion and it gets selected')
  test.equal(sNew.select(), newItem, 'Vote for an unexisting suggestion and it gets added and selected')
  test.equal(sChosen.get().size, 1, 'All tracks chosen are retrieved with "get"')
  
  sChosenNew = sNew.vote(chosenItem).vote(chosenItem)
  test.equal(sChosenNew.select(), chosenItem, 'The suggestion with more votes is the successful one')
  test.equal(sChosenNew.get().size, 2, 'All tracks chosen are retrieved with "get"')

  sCleared = sChosenNew.clear(content)
  test.equal(sCleared.get().get(chosenItem.id).votes, 0, "When a suggestion selection is cleared the chosen item gets 0 votes")
  
  test.done()
}
