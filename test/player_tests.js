if ( global.v8debug ) {
	global.v8debug.Debug.setBreakOnException(); // speaks for itself
}
const list = require('../server/player')

exports.playlist = function(test){
  const track1 = {id:"1", artist:'joe baz', song: 'bar', duration: 30}
  const track2 = {id:"2", artist:'the foos', song: 'bar (remix)', duration:10}

  const pl1 = list()
    .setTrack(0, 30, track1)
    .setTrack(30, 40, track2)

  test.equal(pl1.getTrack(0).track, track1, 'The getTrack method returns the track that is playing for a given time')
  test.equal(pl1.getTrack(0).time, 0, 'The getTrack method returns the track that is playing for a given time')

  test.equal(pl1.getTrack(40).track, track2, 'The getTrack method returns the track that is playing for a given time')
  test.equal(pl1.getTrack(40).time, 10, 'The getTrack method returns the track that is playing for a given time')

  test.equal(pl1.getTrack(41), undefined, 'The getTrack method returns the track that is playing for a given time')
  
  debugger
  test.equal(pl1.tail().size, 2, 'The tail method returns the last tracks from the playlist')

  pl2 = list().setNextTrack(track1, track1.duration).setNextTrack(track2, track2.duration)
  
  test.equal(pl2.getTrack(40).track, track2, 'The getTrack method returns the track that is playing for a given time')



  test.done()
}
