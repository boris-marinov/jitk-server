const Tube = require('youtube-node')
const moment = require('moment')
const q = require('q')

const transformVideo = (video) =>{
  return {
    id: video.id,
    title: video.snippet.title, 
    // description: video.snippet.description,
    duration: moment.duration(video.contentDetails.duration).asMilliseconds(),
    image: video.snippet.thumbnails.standard.url
  }
}

const youTube = new Tube();
youTube.setKey('AIzaSyDcEpNvTRJnkMvTdgbVRgEStTwhYnOepA4');

const getById = q.denodeify(youTube.getById)
const search = q.denodeify(youTube.search)
  
exports.getInfo = (trackId) => {
  return getById(trackId).then((info) => transformVideo(info.items[0])) 
}
exports.search = (query) => {
  return search('query').then((results) => {} //TODO
}

