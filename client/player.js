const React = require('react')
//const YouTubePlayer = require('react-youtube-player')
//const YouTubePlayer = require('./react-youtube-player')
const YouTubePlayer = require('./react-streaming-player').default
module.exports = React.createClass ({
    getInitialState () {
        return {
            playbackState: 'unstarted',
            //videoId: 'M7lc1UVf-VE',
            width: 640,
            height: 360,
            configuration: {
              modestbranding: 1,
              showinfo: 0,
              controls: 0
            }
        };
    },

    render () {
        // console.log(`this.state.playbackState`, this.state.playbackState);

        return <div className='jumbotron'>
            <div 
                className='player-cover'
                style={{width: this.state.width, height: this.state.height}}>
            </div>
            <YouTubePlayer 
                width={this.state.width}
                height={this.state.height}
                videoId={ this.props.track ? this.props.track.id: undefined}
                time={ this.props.time ? this.props.time/1000: undefined}
                playbackState={ this.props.track ? "playing": "unstarted"}
            />

        </div>;
    }
})

