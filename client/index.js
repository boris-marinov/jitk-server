const createStore = require('redux').createStore
const superAgent = require('superagent-q')
const reactRedux = require('react-redux')
const ReactDom = require('react-dom')
const React = require('react')

window.jQuery = require('jquery')
require('bootstrap')

const Player = require('./player')
const Votes = require('./votes')

const reducer = (state, action) => {
  if(action.type === 'UPDATE'){
    console.log(action)
    return action
  }
  return {player:{}, votes:{}}
}


const store = createStore(reducer)

const connection = new WebSocket('ws://' + window.location.host + window.location.pathname)

connection.onmessage = (e) => {
  var msg = JSON.parse(e.data)
  msg.type = "UPDATE"
  store.dispatch(msg)
}


const vote = (dispatch) => ({
              vote: (song) => connection.send(JSON.stringify(song))

            })

const PlayerBound = reactRedux.connect(
            (state) => state.player,
            (dispatch) => ({})
          )(Player)

const VotesBound = reactRedux.connect(
            (state) => ({tracks:state.votes}),
            vote
          )(Votes)

window.onload = () =>
  ReactDom.render(
      <reactRedux.Provider store={store}>
        <div>
          <PlayerBound/>
          <VotesBound/>
        </div>
      </reactRedux.Provider>,
      document.getElementById('root'))
